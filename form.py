#!/usr/bin/python3
from tkinter import *
from threading import Thread
import time

lbdata = []
def donothing():
   filewin = Toplevel(top)
   button = Button(filewin, text="Do nothing button")
   button.pack()

def addData():
    data = text.get()
    Lb1.insert(END, data)
    text.delete(0,len(data))
    
def deleteData():
    index = Lb1.curselection()
    Lb1.delete(index)
    
def updateData():
    index = Lb1.curselection()
    data = text.get()
    text.delete(0,len(data))
    Lb1.delete(index)
    Lb1.insert(END, data)
    
def UpdateBox():
    file = open("dataReceived.txt", "r")
    a = file.readLines()
    

def mainLoop():
    while 1:
        time.sleep(10)
top = Tk()
top.title("Discord Bot - Test Form")

menubar = Menu(top)
filemenu = Menu(menubar, tearoff = 0)
filemenu.add_command(label="New", command = donothing)
filemenu.add_command(label = "Open", command = donothing)
filemenu.add_command(label = "Save", command = donothing)
filemenu.add_command(label = "Save as...", command = donothing)
filemenu.add_command(label = "Close", command = donothing)

filemenu.add_separator()

filemenu.add_command(label = "Exit", command = top.quit)
menubar.add_cascade(label = "File", menu = filemenu)


L1 = Label(top, text = "Comment Log")
L1.place(x = 10,y = 10)

Lb1 = Listbox(top, height=10, width = 100)
Lb1.place(x = 13, y = 30)

Lb1.insert(1, "Data")

L2 = Label(top, text = "Input Data : ")
L2.place(x=10, y = 200)
text = Entry(top)
text.place(x = 80, y = 200)

B = Button(top, text = "Add", command = addData)
B.place(x = 10, y = 230)
B1 = Button(top, text = "Delete", command = deleteData)
B1.place(x = 10, y = 250)

B2 = Button(top, text = "Update", command = updateData)
B2.place(x = 10, y = 270)

top.geometry("800x400")
top.config(menu = menubar)

while True:
    top.update_idletasks()
    top.update()
    time.sleep(1)
    
#top.mainloop()