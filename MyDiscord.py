from random import randint

class Code:
        def __init__(self, code, user):
            self.members = []
            self.codes = code
            self.members.append(user)

        def show(self):
            print("Hello", self.code)

        def addMember(self, user):
            self.members.append(user)

        def showList(self):
            for l in self.members:
                print(l)

class Config:
    TOKEN = ''

class Command:
    def __init__(self, cmd, output):
        self.cmd = cmd
        self.output = output
        
    def printOut(self):
        print(self.output)
        
    def printDisplay(self):
        print("The command name is " + self.cmd + " and the output is " + self.output)
        
class RandCommand:
    def __init__(self, cmd):
        self.cmd = cmd
        self.arr = ["A", "B", "C", "D"]
    
    def printRand(self):
        num = randint(0,len(self.arr)-1)
        print(self.arr[num])
        
    def addData(self, data):
        self.arr.append(data)
        
a = RandCommand("Test")
a.addData("Appended this line")
a.printRand()
