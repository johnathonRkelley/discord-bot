# Work with Python 3.6
# Imports for Bot Script.
import discord
import time
import datetime
from MyDiscord import Config, Code, Command

# Discord Client Init
client = discord.Client()

# Global Variables 
codes = []
randCommands = []
commands = []
startSeconds = 0


# ----File Command Inputs----           
def LoadCommands():
    try:
        file = open("commands.txt", "r")
        
        lines = file.readlines()
        
        for line in lines:
            #print(line)
            
            location = line.index("|")
            
            cmd = line[:location-1]
            output = line[location+2:]
            #print(output)
            #print(cmd)
        
            c = Command(cmd,output)
            #c.printDisplay()
            commands.append(c)
        
        file.close()
        
    except FileNotFound:
        print("File was not found.")

# Loads Random Commands.
def LoadRandomCommands():
    try:
        file = open("randomCommands.txt", "r")
        
    except FileNotFound:
        print("File was not found.")

# Appends data to "Data Recieved File".
def addData(message):
    file = open("dataReceived.txt", 'a')
    file.write(message)
    file.close()

# Checks for Messages that are sent to the Server.
@client.event
async def on_message(message):
    # Adds Data of what was sent
    # addData(message + " sent by " + message.author)

    # No Response to the Bot's Message
    if message.author == client.user:
        return

    # Basic Hello Message
    if message.content.startswith('!hello'):
        await client.send_typing(message.channel)
        msg = 'Hello {0.author.mention}'.format(message)
        await client.send_message(message.channel, msg)

        return
        
    # Command to make Discord Client to disconnect.
    if message.content.startswith('!close'):
        print("should be closing soon")
        await client.send_message(message.channel, "Bot is closing now...")

        client.loop.stop()
        print("Loop is stopped now.")
        return

    if message.content == '!a':
        channel = message.channel
        print("Members location : " +str(channel.id))

        return

    # Gets from list of Commands.
    for line in commands:
        c = line
        if c.cmd == message.content:
            msg = '{0.author.mention}, '.format(message) + c.output
            await client.send_message(message.channel, msg)
            
            print(c.output)
            return

    # Testing of Stream Sniper Code Grouping Using Members Channel.
    if message.channel.name.startswith('members'):

        # Checks to see valid members.        
        if message.content.startswith("!members"):
            for c in codes:
                print("In this the code : "  + c.codes)
                c.showList()

        else: #If anything else, record it.
            #Grabs Message Content and Author 
            current = message.content
            author = str(message.author)
            print("Was sent by : " + author + " | Content : " + current)
            
            # 1st case checks for empty Codes Object.
            if len(codes) == 0:
                codes.append(Code(current, author))
                print("Added current code to empty list " + current + ".")
                startSeconds = message.timestamp.timestamp()
            else: # Checks for Valid Code.            
                print("List isn't empty so we need to check if code is in here : " + current)
                found = False        
                for c in codes:
                    print(c.codes + " == " + current)
                    if c.codes is current:
                        found = True
                        c.addMember(author)
                        print(current + " is found. " + str(found))
                    
                if found is False:
                    print(current + " was not found... creating new code.")
                    codes.append(Code(current, author))

            print("This is how big the array is currently is " + str(len(codes)))
        
# When Client Loads into Server.
@client.event
async def on_ready():
    #Basic Information on Load in.
    print('Logged in as')
    print("Client Name : " +  client.user.name)
    print("Client ID : " + client.user.id)
    print('------')

    #---- Start Number of Members Stats ----
    total = 0
    for server in client.servers:
        for member in server.members:
            total = total + 1

    a = "members-" + str(total)

    c1 = None
    for server in client.servers:
        print("Server ID : " + server.id)
        for channel in server.channels:
            if channel.name.startswith("member"):
                c1 = channel

    #Channel Number for Members Stats - 555385505177534464
    #Server Number - 481211742534500354

    print(c1)
    print(client.get_channel(555385505177534464))
    await client.edit_channel(c1, name=a)
    #---- End Number of Members Stats ----

    print(time.clock())
    print('\n\n\n')
    
    
LoadCommands()
#client.run(Config.TOKEN)  #runs the on_ready

try:
    client.loop.run_until_complete(client.start(Config.TOKEN))

except:
    print("Keyboard Exception")
    client.loop.run_until_complete(client.logout())
    # cancel all tasks lingering
finally:
    client.loop.close()