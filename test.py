import time
from random import randint

class Code:  
    def __init__(self, code):   
        self.members = []
        self.codes = code
        self.members.append("Player")
        
    def show(self):    
        print("Hello",self.code)   
        
    def showList(self):
        for l in self.members:
            print(l)

testcodes = []

num = 0

while num < 3:
    current = ""
    
    number = randint(0,1)
    
    if number == 0:
        current = "abc"
    else:
        current = "xyz"
    
    print("Current Code " + current + " | " + str(number))

    
    if len(testcodes) == 0:
            testcodes.append(Code(current))
            print("Added current code to empty list " + current + ".")
    else:
        print("List isn't empty so we need to check if code is in here : " + current)
        found = False        
        for c in testcodes:
            print(c.codes + " == " + current)
            if c.codes is current:
                c.members.append("Player")
                found = True
                print(current + " is found. " + str(found))
                    
        if found is False:
            print(current + " was not found... creating new code.")
            testcodes.append(Code(current))
        
    num = num + 1
    for c in testcodes:
        print("["+c.codes+"]," + str(len(c.members)))
        
    print("\n\n")
    
print("Printing Members")

print(len(testcodes))