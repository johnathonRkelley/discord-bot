# Discord Bot

## Main Purpose of this Bot
1. Get a better understanding of using Python thru creating an automated program.
2. Learn how to use pre-built modules/APIs to connect, send messages, monitor and moderate chat in a Discord server.
3. Understand how a Discord server actaully works. 

## Goals of the project
1. Create a bot that helps me maintain my Discord server.
2. Allow for redeployability (others can use my program to have a bot for their own servers.

## Ideas for the project to be implemented
- changing channel names to show current stats of discord server
- groups people based on certain codes used in the server
- have predetermined audio to be played in a voice channel when a command is sent.
- welcome users who join the server. 
- have preset commands that a user can use to get information about the server. 

## PROGRESS
3.16.2019 - got Python downloaded and working
3.17.2019 - Added implementation to show user stats on chanmel name "members".
3.20.2019 - identified error that was causing code in test.py to produce all the same variables.
3.21.2019 - Implemented Stream Snipe Grouping Through Inputed Codes